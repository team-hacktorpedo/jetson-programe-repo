import numpy as np
import cv2

winname = "Test"
cv2.namedWindow(winname)        # Create a named window
cv2.moveWindow(winname, 40,30)  # Move it to (40,30)

# read input
cap = cv2.VideoCapture('CarParkDetection/Data/Auto Parkt Vorwärts Neben Anderem Auto Und Bleibt Stehen-1.m4v')
if (cap.isOpened()== False):
  print("Error opening video stream or file")

#img = cv2.imread("CarParkDetection/Data/Street.jpg")

# specify desired output size and Parking lots

width = 1020
height = 1020

parplatz = np.array([(-150,-80),(150,80)])

parplaetze = np.array([(163,130),
                       (860,142),
                       (163,331),
                       (860,340),
                       (163,523),
                       (860,540),
                       (163,727),
                       (860,742),
                       (163,935),
                       (860,931)])

# specify conjugate x,y coordinates (not y,x)
input = np.float32([[406,420], [1504,400], [1960,1002], [-28,1020]])
output = np.float32([[0,0], [width-1,0], [width-1,height-1], [0,height-1]])

# compute perspective matrix
matrix = cv2.getPerspectiveTransform(input,output)

while(cap.isOpened()):
    ret, frame = cap.read()
    if ret == True:
        img = frame

        # do perspective transformation setting area outside input to black
        imgOutput = cv2.warpPerspective(img, matrix, (width,height), cv2.INTER_LINEAR, borderMode=cv2.BORDER_CONSTANT, borderValue=(0,0,0))

        blk = np.zeros(imgOutput.shape, np.uint8)

        #with np.nditer(parplaetze, op_flags=['readwrite']) as it:
        for row in parplaetze:
            print(row)
            print("p", row + parplatz[0], "c", row + parplatz[1])
            PointA = row + parplatz[0]
            PointB = row + parplatz[1]

            cv2.rectangle(blk, (PointA[0], PointA[1]),  (PointB[0], PointB[1]), (0, 255, 0), cv2.FILLED)

        imgOutput = cv2.addWeighted(imgOutput, 1.0, blk, 0.6, 1)
        print(imgOutput.shape)

        # save the warped output
        #cv2.imwrite("TransformedParkplatz.jpg",imgOutput)

        # show the result
        cv2.imshow(winname, imgOutput)
        if cv2.waitKey(5) & 0xFF == ord('q'):
            break

    else: 
        break
        #cv2.waitKey(0)
